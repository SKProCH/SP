﻿using System;
using System.Globalization;
using System.Threading;

namespace SP.Model {
    
    public class TimeProvider : PropertyChangedBase {
        public readonly DateTime MarathonDate = DateTime.ParseExact("2020-06-26 06:00", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture);
        private Timer _timer;
        public static TimeProvider Instance { get; set; } = new TimeProvider();
        
        public string TimeRemaining { get; set; }
        public string CurrentDate { get; set; }

        public TimeProvider() {
            _timer = new Timer(state => {
                CurrentDate = DateTime.Now.ToString("dddd dd MMM yyyy");
                var remain = (MarathonDate - DateTime.Now);
                TimeRemaining = $"{(int)remain.TotalDays} дней, {remain.Hours} часов, {remain.Minutes} минут до старта марафона!";
            }, null, TimeSpan.FromSeconds(0), TimeSpan.FromSeconds(5));
        }
    }
}