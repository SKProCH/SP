﻿using System.Windows.Controls;

namespace SP.Model {
    public interface IHowLongMarathonObject {
        string ObjectImage { get; set; }
        string ObjectName { get; set; }
        string ComparableDescription { get; }
    }

    public class SpeedMarathonObject : IHowLongMarathonObject {
        public SpeedMarathonObject(double speed, string objectImage, string objectName) {
            Speed = speed;
            ObjectImage = objectImage;
            ObjectName = objectName;
        }

        private double Speed { get; set; }
        public string ObjectImage { get; set; }
        public string ObjectName { get; set; }
        public string ComparableDescription => $"Максимальная скорость {ObjectName} - {Speed} км/ч. Это займет {(int)(42 / Speed * 60)} минут, чтобы завершить полный марафон в 42 км";
    }

    public class DistanceMarathonObject : IHowLongMarathonObject {
        public DistanceMarathonObject(double length, string objectImage, string objectName) {
            Length = length;
            ObjectImage = objectImage;
            ObjectName = objectName;
        }

        private double Length { get; set; }
        public string ObjectImage { get; set; }
        public string ObjectName { get; set; }
        public string ComparableDescription => $"Длина {ObjectName} - {Length} м. Необходимо задействовать {(int)(42000 / Length)} из них, чтобы покрыть расстояние марафона длиной в 42 км";
    }
}