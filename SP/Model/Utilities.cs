﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SP.Model {
    public static class Utilities {
        public static IEnumerable<TResult> SafeSelect<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector) {
            var results = new List<TResult>();
            foreach (var source1 in source.ToList()) {
                try {
                    results.Add(selector.Invoke(source1));
                }
                catch (Exception) {
                    // ignored
                }
            }

            return results;
        }
    }
}