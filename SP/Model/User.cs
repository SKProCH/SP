﻿namespace SP.Model {
    public class User {
        public User() { }

        public User(string input) {
            var strings = input.Split('|');
            Email = strings[0];
            FirstName = strings[1];
            LastName = strings[2];
            Role = strings[3];
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
    }
}