﻿namespace SP.Model {
    public class UserCredentials {
        public string Login { get; set; }
        public string Password { get; set; }

        public UserType Type { get; set; }
    }

    public enum UserType {
        Runner,
        Coordinator,
        Admin
    }
}