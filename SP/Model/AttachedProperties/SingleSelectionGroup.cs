﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace SP.Model.AttachedProperties {
    public static class SingleSelectionGroup {
        public delegate void SingleSelectionChangedHandler(SingleSelectionChangedEventArgs e);

        public static readonly DependencyProperty GroupNameProperty =
            DependencyProperty.RegisterAttached("GroupName", typeof(string), typeof(SingleSelectionGroup),
                new UIPropertyMetadata(OnGroupNameChanged));

        private static readonly Dictionary<string, List<ListBox>> Groups = new Dictionary<string, List<ListBox>>();

        private static readonly Dictionary<string, Dictionary<string, Action<string>>> ListToRaise =
            new Dictionary<string, Dictionary<string, Action<string>>>();

        public static string GetGroupName(Selector selector) {
            return (string) selector.GetValue(GroupNameProperty);
        }

        public static void SetGroupName(Selector selector, string value) {
            selector.SetValue(GroupNameProperty, value);
        }

        [Localizable(false)]
        public static object GetSelectedContentByGroup(string groupName) {
            return !Groups.ContainsKey(groupName) ? null : Groups[groupName].FirstOrDefault(x => x.SelectedIndex != -1)?.SelectedItem;
        }

        public static ListBox GetSelectedListBox(string groupName) {
            return !Groups.ContainsKey(groupName) ? null : Groups[groupName].FirstOrDefault(x => x.SelectedIndex != -1);
        }

        public static string GetSelectedListBoxName(string groupName) {
            return GetSelectedListBox(groupName).Name;
        }

        private static void OnGroupNameChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e) {
            var lb = (ListBox) dependencyObject;

            lb.Unloaded += (sender, args) => {
                try {
                    Groups[GetGroupName(lb)].Remove(lb);
                    if (Groups[GetGroupName(lb)].Count == 0)
                        Groups.Remove(GetGroupName(lb));
                }
                catch (Exception) {
                    // ignored
                }
            };
            var selector = (Selector) dependencyObject;

            if (e.OldValue != null) {
                selector.SelectionChanged -= SelectorOnSelectionChanged;
                Groups[e.OldValue.ToString()].Remove(lb);
                if (Groups[e.OldValue.ToString()].Count == 0)
                    Groups.Remove(e.OldValue.ToString());
                TryRaise(e.OldValue.ToString());
            }

            if (e.NewValue == null) return;
            selector.SelectionChanged += SelectorOnSelectionChanged;

            if (!Groups.ContainsKey(e.NewValue.ToString()))
                Groups.Add(e.NewValue.ToString(), new List<ListBox>());

            Groups[e.NewValue.ToString()].Add(lb);
            TryRaise(e.NewValue.ToString());
        }

        public static event SingleSelectionChangedHandler OnSelectionChanged;

        private static void SelectorOnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (e.AddedItems.Count == 0)
                return;

            var selector = (Selector) sender;
            var groupName = (string) selector.GetValue(GroupNameProperty);
            var groupSelectors = GetGroupSelectors(selector, groupName);

            foreach (var groupSelector in groupSelectors.Where(gs => !gs.Equals(sender)))
                groupSelector.SelectedIndex = -1;

            OnSelectionChanged?.Invoke(new SingleSelectionChangedEventArgs(groupName, e.AddedItems));
            TryRaise(groupName);
        }

        private static IEnumerable<Selector> GetGroupSelectors(DependencyObject selector, string groupName) {
            var selectors = new Collection<Selector>();
            var parent = GetParent(selector);
            GetGroupSelectors(parent, selectors, groupName);
            return selectors;
        }

        private static DependencyObject GetParent(DependencyObject depObj) {
            while (true) {
                var parent = VisualTreeHelper.GetParent(depObj);
                if (parent == null) return depObj;
                depObj = parent;
            }
        }

        private static void GetGroupSelectors(DependencyObject parent, Collection<Selector> selectors, string groupName) {
            var childrenCount = VisualTreeHelper.GetChildrenCount(parent);

            for (var i = 0; i < childrenCount; i++) {
                var child = VisualTreeHelper.GetChild(parent, i);
                if (child is Selector selector && (string) selector.GetValue(GroupNameProperty) == groupName)
                    selectors.Add(selector);

                GetGroupSelectors(child, selectors, groupName);
            }
        }

        public static void AddPropertyNameToRaise(string group, string propertyName, Action<string> action = null) {
            if (!ListToRaise.ContainsKey(group))
                ListToRaise.Add(group, new Dictionary<string, Action<string>>());

            try {
                ListToRaise[group].Add(propertyName, action);
            }
            catch (Exception) {
                // ignored
            }
        }

        public static void RemovePropertyNameToRaise(string group, string propertyName) {
            try {
                ListToRaise[group].Remove(propertyName);

                if (ListToRaise[group].Count == 0)
                    ListToRaise.Remove(group);
            }
            catch (Exception) {
                // ignored
            }
        }

        private static void TryRaise(string groupName) {
            ListToRaise.FirstOrDefault(x => x.Key == groupName).Value?.ToList().ForEach(pair => pair.Value?.Invoke(pair.Key));
        }

        public sealed class SingleSelectionChangedEventArgs {
            public SingleSelectionChangedEventArgs(string groupName, IList newSelectedObject) {
                GroupName = groupName;
                NewSelectedObject = newSelectedObject;
            }

            public string GroupName { get; }
            public IList NewSelectedObject { get; }
        }
    }
}