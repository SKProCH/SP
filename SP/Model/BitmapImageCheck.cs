﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Microsoft.Win32;

namespace SP.Model {
    public sealed class BitmapImageCheck : IDisposable {
        #region constructors

        [Localizable(false)]
        public static string GetFilters() {
            if (_filter != null) return _filter;
            _filter = @"Image Files|";
            foreach (var supportedExtension in new BitmapImageCheck().AllSupportedExtensions) _filter += "*" + supportedExtension + ";";

            return _filter;
        }
        
        public BitmapImageCheck() {
            string baseKeyPath;
            if (Environment.Is64BitOperatingSystem && !Environment.Is64BitProcess)
                baseKeyPath = "Wow6432Node\\CLSID";
            else
                baseKeyPath = "CLSID";
            _baseKey = Registry.ClassesRoot.OpenSubKey(baseKeyPath, false);
            RecalculateExtensions();
        }

        #endregion

        #region public methods

        /// <summary>
        ///     Check whether a file is likely to be supported by BitmapImage based upon its extension
        /// </summary>
        /// <param name="extension">File extension (with or without leading full stop), file name or file path</param>
        /// <returns>True if extension appears to contain a supported file extension, false if no suitable extension was found</returns>
        public bool IsExtensionSupported(string extension) {
            //prepare extension, should a full path be given
            if (extension.Contains(".")) extension = extension.Substring(extension.LastIndexOf('.') + 1);
            extension = extension.ToUpper();
            extension = extension.Insert(0, ".");
            return AllSupportedExtensions.Contains(extension);
        }

        #endregion

        #region class variables

        private readonly RegistryKey _baseKey;
        private static string _filter;
        private const string WicDecoderCategory = "{7ED96837-96F0-4812-B211-F13C24117ED3}";

        #endregion

        #region properties

        /// <summary>
        ///     File extensions that are supported by decoders found elsewhere on the system
        /// </summary>
        public string[] CustomSupportedExtensions { get; private set; }

        /// <summary>
        ///     File extensions that are supported natively by .NET
        /// </summary>
        public string[] NativeSupportedExtensions { get; private set; }

        /// <summary>
        ///     File extensions that are supported both natively by NET, and by decoders found elsewhere on the system
        /// </summary>
        public string[] AllSupportedExtensions { get; private set; }

        #endregion

        #region private methods

        /// <summary>
        ///     Re-calculate which extensions are available on this system. It's unlikely this ever needs to be called outside of
        ///     the
        ///     constructor.
        /// </summary>
        private void RecalculateExtensions() {
            CustomSupportedExtensions = GetSupportedExtensions().ToArray();
            NativeSupportedExtensions = new[] {".BMP", ".GIF", ".ICO", ".JPEG", ".PNG", ".TIFF", ".DDS", ".JPG", ".JXR", ".HDP", ".WDP"};

            var cse = CustomSupportedExtensions;
            var nse = NativeSupportedExtensions;
            var ase = new string[cse.Length + nse.Length];
            Array.Copy(nse, ase, nse.Length);
            Array.Copy(cse, 0, ase, nse.Length, cse.Length);
            AllSupportedExtensions = ase;
        }

        /// <summary>
        ///     Represents information about a WIC decoder
        /// </summary>
        private struct DecoderInfo {
            public string FriendlyName;
            public string FileExtensions;
        }

        /// <summary>
        ///     Gets a list of additionally registered WIC decoders
        /// </summary>
        /// <returns></returns>
        private IEnumerable<DecoderInfo> GetAdditionalDecoders() {
            var result = new List<DecoderInfo>();

            foreach (var codecKey in GetCodecKeys()) {
                var decoderInfo = new DecoderInfo();
                decoderInfo.FriendlyName = Convert.ToString(codecKey.GetValue("FriendlyName", ""));
                decoderInfo.FileExtensions = Convert.ToString(codecKey.GetValue("FileExtensions", ""));
                result.Add(decoderInfo);
            }

            return result;
        }

        private List<string> GetSupportedExtensions() {
            var decoders = GetAdditionalDecoders();
            return decoders.SelectMany(decoder => decoder.FileExtensions.Split(',')).ToList();
        }

        private IEnumerable<RegistryKey> GetCodecKeys() {
            var result = new List<RegistryKey>();

            if (_baseKey == null) return result;
            var categoryKey = _baseKey.OpenSubKey(WicDecoderCategory + "\\instance", false);
            if (categoryKey == null) return result;
            // Read the guids of the registered decoders
            var codecGuids = categoryKey.GetSubKeyNames();
            result.AddRange(GetCodecGuids().Select(codecGuid => _baseKey.OpenSubKey(codecGuid)).Where(codecKey => codecKey != null));
            return result;
        }

        private IEnumerable<string> GetCodecGuids() {
            var categoryKey = _baseKey?.OpenSubKey(WicDecoderCategory + "\\instance", false);
            return categoryKey?.GetSubKeyNames();
        }

        #endregion

        #region overrides and whatnot

        public override string ToString() {
            var result = "";

            result += "\nNative support for the following extensions is available: ";
            result = NativeSupportedExtensions.Aggregate(result, (current, item) => current + item + ",");

            if (NativeSupportedExtensions.Any())
                result = result.Remove(result.Length - 1);

            var decoders = GetAdditionalDecoders();
            var decoderInfos = decoders.ToList();

            if (!decoderInfos.Any()) {
                result += "\n\nNo custom decoders found.";
            }
            else {
                result += "\n\nThese custom decoders were also found:";
                result = decoderInfos.Aggregate(result,
                    (current, decoder) => current + "\n" + decoder.FriendlyName + ", supporting extensions " + decoder.FileExtensions);
            }

            return result;
        }

        public void Dispose() {
            _baseKey.Dispose();
        }

        #endregion
    }
}