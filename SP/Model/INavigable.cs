﻿using System;
using SP.Model;

namespace SP.Model {
    public interface INavigable {
        object PreviousContent { get; set; }
        void OnClosing();
        event EventHandler<object> Navigate;
    }

    
    public abstract class Navigable : PropertyChangedBase, INavigable {
        public object PreviousContent { get; set; }
        public void OnClosing() { }
        public event EventHandler<object> Navigate;

        protected virtual void ExecuteReturn() {
            Navigate?.Invoke(this, PreviousContent);
        }
        
        protected virtual void ExecuteNavigate(object e = null) {
            Navigate?.Invoke(this, e);
        }
    }
}