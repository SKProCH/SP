﻿using System.Globalization;
using System.Windows.Controls;

namespace SP.Model.ValidationRules {
    public class PositiveValueValidationRule : ValidationRule {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo) {
            if (int.TryParse(value?.ToString(), out var i)) {
                return i >= 0 ? ValidationResult.ValidResult : new ValidationResult(false, "Значение не может быть отрицательным");
            }
            else {
                return new ValidationResult(false, "Значение не число");
            }
        }
    }
}