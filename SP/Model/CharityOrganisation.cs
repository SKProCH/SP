﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Media.Imaging;
using Image = System.Windows.Controls.Image;

namespace SP.Model {
    public class CharityOrganisation : PropertyChangedBase {
        // По хорошему, если мне всеравно нужно вставлять лого для День 12
        // То этот класс тоже нужно отрефакторить
        // И заставить ссылаться на изображения, а не хранить их в BASE64
        // Но мне лень переделывать

        // Но учитывая тот пиздец, что в методичке вообще нужно напрямую их из ресурсов черпать, относительным путем
        // То на чистоту проекта мне уже как то пох

        // Собственно, если авторы задания кайфуют от скорости скачивания 200 метрового проекта с GitLab - их выбор
        // Не мне его скачивать нужно для каждого дня :)

        // Мне лень запихивать каждую организацию в правильный JSON, и потом его парсить, поэтому будет костыль
        // Ну и хардкодить мне тоже лень, поэтому есть этот класс
        private CharityOrganisation() { }
        public string Name { get; set; }
        public string Description { get; set; }
        public BitmapImage Logo { get; set; }

        public static CharityOrganisation ParseFileOrg(string encoded) {
            var charityOrganisation = new CharityOrganisation();
            var strings = encoded.Split('\n');
            var binaryData = Convert.FromBase64String(strings[0]);

            var bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = new MemoryStream(binaryData);
            bi.EndInit();
            charityOrganisation.Logo = bi;

            charityOrganisation.Name = strings[1];
            charityOrganisation.Description = string.Join("\n", strings.Skip(2));
            return charityOrganisation;
        }

        public static CharityOrganisation ParseCharityCsvLine(string encoded) {
            var charityOrganisation = new CharityOrganisation();

            var strings = encoded.Split('|');
            charityOrganisation.Name = strings[1];
            charityOrganisation.Description = strings[2];
            try {
                charityOrganisation.Logo = new BitmapImage(new Uri("pack://application:,,,/SP;component/Resources/Charities/" + strings[3]));
                
            }
            catch (Exception) {
                charityOrganisation.Logo = new BitmapImage(new Uri(strings[3]));
            }
            
            return charityOrganisation;
        }

        public static CharityOrganisation Create(string name, string description, string logoPath) {
            var organisation = new CharityOrganisation {Name = name, Description = description};
            try {
                organisation.Logo = new BitmapImage(new Uri(logoPath));
            }
            catch (Exception) {
                organisation.Logo = new BitmapImage();
            }

            return organisation;
        }
    }
}