﻿using System;

namespace SP.Model {
    public class Sponsorship : PropertyChangedBase {
        private bool _isEnabled = true;
        public event EventHandler EnabledChanged;
        public Sponsorship() { }

        public Sponsorship(string toParse) {
            var strings = toParse.Split(',');
            Id = Convert.ToInt32(strings[0]);
            Name = strings[1];
            Year = strings[2];
            Amount = double.Parse(strings[3], System.Globalization.CultureInfo.InvariantCulture);
        }

        public string Year { get; set; }
        public double Amount { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }

        public bool IsEnabled {
            get => _isEnabled;
            set {
                _isEnabled = value;
                OnEnabledChanged();
            }
        }

        protected virtual void OnEnabledChanged() {
            EnabledChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}