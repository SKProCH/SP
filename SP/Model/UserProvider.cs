﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;

namespace SP.Model {
    public static class UserProvider {
        static UserProvider() {
            Users.Add(new UserCredentials {Login = "r.richmond@ew.net", Password = "AS0QqFvI!", Type = UserType.Admin});
            Users.Add(new UserCredentials {Login = "pklang210@gmail.com", Password = "YeF7hZ6@", Type = UserType.Coordinator});
            Users.Add(new UserCredentials {Login = "a.adkin@dayrep.net", Password = "jwZh2x@p", Type = UserType.Runner});
        }
        private static List<UserCredentials> Users { get; set; } = new List<UserCredentials>();
        public static List<UserCredentials> GetUsers() {
            return Users.ToList();
        }

        public static UserCredentials Login(string login, string password) {
            return Users.FirstOrDefault(credentials => credentials.Login == login && credentials.Password == password);
        }

        public static string ValidatePassword(string password) {
            var s = new StringBuilder();
            if (password.Length < 6 || password.Length > 12) {
                s.AppendLine("От 6 до 12 символов");
            }

            if (password.ToLower() == password) {
                s.AppendLine("Минимум 1 прописная буква");
            }

            if (password.ToUpper() == password) {
                s.AppendLine("Минимум 1 строчная буква");
            }

            if (!password.Any(char.IsDigit)) {
                s.AppendLine("Минимум 1 цифра");
            }

            if (!password.Any(c => "!@#$^".Contains(c))) {
                s.AppendLine("По крайней мере один из следующих символов: ! @ # $ % ^");
            }

            return s.Length == 0 ? null : "Требования к паролю:\n" + s;
        }
    }
}