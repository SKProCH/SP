﻿using System;

namespace SP.Model {
    public interface IAlertable {
        event EventHandler<string> Alert;
    }
}