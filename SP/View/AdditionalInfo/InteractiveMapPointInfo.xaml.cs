﻿using System.Windows;
using System.Windows.Controls;

namespace SP.View.AdditionalInfo {
    public partial class InteractiveMapPointInfo : UserControl {
        public InteractiveMapPointInfo(string pointName, string landMark, bool drinks, bool bars, bool toilets, bool info, bool med) {
            InitializeComponent();
            PointName.Text = pointName;
            Landmark.Text = landMark;
            Drinks.Visibility = drinks ? Visibility.Visible : Visibility.Collapsed;
            Bars.Visibility = bars ? Visibility.Visible : Visibility.Collapsed;
            Toilets.Visibility = toilets ? Visibility.Visible : Visibility.Collapsed;
            Info.Visibility = info ? Visibility.Visible : Visibility.Collapsed;
            Medical.Visibility = med ? Visibility.Visible : Visibility.Collapsed;
        }
    }
}