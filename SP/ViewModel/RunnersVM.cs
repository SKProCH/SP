﻿using System.Windows;
using SP.Model;

namespace SP.ViewModel {
    public class RunnersVM : PropertyChangedBase {
        public TimeProvider TimeProvider { get; set; } = TimeProvider.Instance;
        public AnotherCommandImplementation Close => new AnotherCommandImplementation(o => {
            if (o is Window window) {
                window.Close();
            }
        });
    }
}