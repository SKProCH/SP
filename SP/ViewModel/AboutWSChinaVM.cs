﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using Microsoft.Win32;
using SP.Model;

namespace SP.ViewModel {
    public class AboutWSChinaVM : PropertyChangedBase {
        public AboutWSChinaVM() {
            GroupsList = Directory.GetDirectories("Resources/China").Select(Path.GetFileName).ToList();
        }
        private string _selectedGroup;

        public AnotherCommandImplementation Close => new AnotherCommandImplementation(o => {
            if (o is Window window) {
                window.Close();
            }
        } );

        public TimeProvider TimeProvider { get; set; } = TimeProvider.Instance;

        public Dictionary<string, string> CurrentContentList { get; set; } = new Dictionary<string, string>();

        public List<string> GroupsList { get; set; }

        public AnotherCommandImplementation Download => new AnotherCommandImplementation(o => {
            var fileInfo = new FileInfo(o.ToString());
            var dialog = new SaveFileDialog {InitialDirectory = fileInfo.FullName, FileName = Path.GetFileName(fileInfo.FullName)};
            if (dialog.ShowDialog() == true) {
                File.Copy(fileInfo.FullName, dialog.FileName);
            }
        });

        public string SelectedGroup {
            get => _selectedGroup;
            set {
                _selectedGroup = value;
                CurrentContentList = Directory.GetFiles(Path.Combine("Resources/China", value)).ToDictionary(Path.GetFileName, s => s);
            }
        }
    }
}