﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using SP.Model;
using SP.Model.AttachedProperties;

namespace SP.ViewModel.AdditionalInfo {
    public class HowLongMarathonVM : Navigable {
        public HowLongMarathonVM() {
            SingleSelectionGroup.OnSelectionChanged += args => {
                if (args.GroupName == "HowLongMarathon") {
                    SelectedObject = (IHowLongMarathonObject) args.NewSelectedObject[0];
                }
            };
            
            DistanceObjects.Add(new DistanceMarathonObject(10, "pack://application:,,,/SP;component/Resources/HowLong/bus.jpg", "Автобус"));
            DistanceObjects.Add(new DistanceMarathonObject(0.245, "pack://application:,,,/SP;component/Resources/HowLong/pair-of-havaianas.jpg", "Тапки Havaianas"));
            DistanceObjects.Add(new DistanceMarathonObject(73, "pack://application:,,,/SP;component/Resources/HowLong/airbus-a380.jpg", "Airbus A380"));
            DistanceObjects.Add(new DistanceMarathonObject(105, "pack://application:,,,/SP;component/Resources/HowLong/football-field.jpg", "Футбольное поле"));
            DistanceObjects.Add(new DistanceMarathonObject(1.81, "pack://application:,,,/SP;component/Resources/HowLong/ronaldinho.jpg", "Рональдиньо"));
            
            SpeedObjects.Add(new SpeedMarathonObject(345, "pack://application:,,,/SP;component/Resources/HowLong/f1-car.jpg", "Болид \"Формулы-1\""));
            SpeedObjects.Add(new SpeedMarathonObject(0.01, "pack://application:,,,/SP;component/Resources/HowLong/slug.jpg", "Слизень"));
            SpeedObjects.Add(new SpeedMarathonObject(15, "pack://application:,,,/SP;component/Resources/HowLong/horse.png", "Лошадь"));
            SpeedObjects.Add(new SpeedMarathonObject(0.12, "pack://application:,,,/SP;component/Resources/HowLong/sloth.jpg", "Ленивец"));
            SpeedObjects.Add(new SpeedMarathonObject(35, "pack://application:,,,/SP;component/Resources/HowLong/capybara.jpg", "Капибара"));
            SpeedObjects.Add(new SpeedMarathonObject(80, "pack://application:,,,/SP;component/Resources/HowLong/jaguar.jpg", "Ягуар"));
            SpeedObjects.Add(new SpeedMarathonObject(0.03, "pack://application:,,,/SP;component/Resources/HowLong/worm.jpg", "Червь"));
        }
        
        public List<DistanceMarathonObject> DistanceObjects { get; set; } = new List<DistanceMarathonObject>();
        public List<SpeedMarathonObject> SpeedObjects { get; set; } = new List<SpeedMarathonObject>();
        
        public IHowLongMarathonObject SelectedObject { get; set; }
    }
}