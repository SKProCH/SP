﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using MaterialDesignThemes.Wpf;
using SP.Model;
using SP.View.AdditionalInfo;

namespace SP.ViewModel.AdditionalInfo {
    public class InfoAboutVM : Navigable {
        public AnotherCommandImplementation ExpandImage =>
            new AnotherCommandImplementation(o => {
                var res = new InfoAbout();
                var flatResource = res.FindResource("MaterialDesignFlatButton");
                var bitmapImage = new BitmapImage(new Uri(o.ToString()));
                DialogHost.Show(
                    new Button {
                        Width = bitmapImage.Width,
                        Height = bitmapImage.Height,
                        Content = new Image {Source = bitmapImage},
                        Style = (Style) flatResource,
                        Command = DialogHost.CloseDialogCommand
                    }, "InfoAbout");
            });

        public AnotherCommandImplementation OpenInteractiveMap => new AnotherCommandImplementation(o => {
            ExecuteNavigate(new InteractiveMap {DataContext = new InteractiveMapVM()});
        });
    }
}