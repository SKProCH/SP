﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using SP.Model;

namespace SP.ViewModel.AdditionalInfo {
    public class CharityOrganisationsVM : Navigable {
        public CharityOrganisationsVM() {
            CharityOrganisationsList.AddRange(Assembly.GetExecutingAssembly().GetManifestResourceNames().Where(s => s.Contains(".org"))
                                                      .Select(s => CharityOrganisation.ParseFileOrg(LoadFromResources(s))));
        }

        public string LoadFromResources(string path) {
            var assembly = Assembly.GetExecutingAssembly();

            using var stream = assembly.GetManifestResourceStream(path);
            using var reader = new StreamReader(stream);
            return reader.ReadToEnd();
        }

        public List<CharityOrganisation> CharityOrganisationsList { get; set; } = new List<CharityOrganisation>();
    }
}