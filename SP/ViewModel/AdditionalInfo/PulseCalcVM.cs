﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using MaterialDesignThemes.Wpf;
using PropertyChanged;
using SP.Model;
using SP.View.AdditionalInfo;

namespace SP.ViewModel.AdditionalInfo {
    public class PulseCalcVM : Navigable {
        [AlsoNotifyFor(nameof(PulseRate))] public int RestPulse { get; set; } = 70;

        [AlsoNotifyFor(nameof(PulseRate))] public int Age { get; set; } = 18;

        public int PulseRate => 220 - Age - RestPulse;

        public AnotherCommandImplementation ExpandImage => new AnotherCommandImplementation(o => {
            var res = new PulseCalc();
            var flatResource = res.FindResource("MaterialDesignFlatButton");
            var bitmapImage = new BitmapImage(new Uri(o.ToString()));
            DialogHost.Show(
                new Button {
                    Width = bitmapImage.Width,
                    Height = bitmapImage.Height,
                    Content = new Image {Source = bitmapImage},
                    Style = (Style) flatResource,
                    Command = DialogHost.CloseDialogCommand
                });
        });

        [DependsOn(nameof(PulseRate))] public int Value0 => (int) (PulseRate * 0.5 + RestPulse);
        [DependsOn(nameof(PulseRate))] public int Value1 => (int) (PulseRate * 0.6 + RestPulse);
        [DependsOn(nameof(PulseRate))] public int Value2 => (int) (PulseRate * 0.7 + RestPulse);
        [DependsOn(nameof(PulseRate))] public int Value3 => (int) (PulseRate * 0.8 + RestPulse);
        [DependsOn(nameof(PulseRate))] public int Value4 => (int) (PulseRate * 0.9 + RestPulse);
        [DependsOn(nameof(PulseRate))] public int Value5 => (int) (PulseRate * 1.0 + RestPulse);
    }
}