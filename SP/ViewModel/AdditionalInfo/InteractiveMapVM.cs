﻿using System;
using System.Collections.Generic;
using System.Windows.Documents;
using SP.Model;
using SP.View.AdditionalInfo;

namespace SP.ViewModel.AdditionalInfo {
    public class InteractiveMapVM : Navigable {
        public InteractiveMapVM() {
            for (var i = 0; i < 3; i++) {
                StartsInfo.Add(ConstructStartPreview(i));
            }
            for (var i = 0; i < 8; i++) {
                CheckpointInfo.Add(ConstructCheckpointPreview(i));
            }
        }
        public object CurrentContent { get; set; }
        public List<Lazy<object>> StartsInfo { get; set; } = new List<Lazy<object>>();
        public List<Lazy<object>> CheckpointInfo { get; set; } = new List<Lazy<object>>();

        public AnotherCommandImplementation ShowStartInfo => new AnotherCommandImplementation(o => {
            CurrentContent = StartsInfo[int.Parse(o.ToString())].Value;
        });

        public AnotherCommandImplementation ShowCheckpointInfo => new AnotherCommandImplementation(o => {
            CurrentContent = CheckpointInfo[int.Parse(o.ToString())].Value;
        });

        public Lazy<object> ConstructStartPreview(int i) {
            return new Lazy<object>(() => {
                var landmark = i switch {
                    0 => "42км Полный марафон",
                    1 => "21км Полумарафон",
                    2 => "5км Малая дистанция"
                };

                return new InteractiveMapPointInfo("Начало гонки", landmark, false, false, false, false, false);
            });
        }

        public Lazy<object> ConstructCheckpointPreview(int i) {
            return new Lazy<object>(() => {
                var name = "Checkpoint " + (i + 1);

                return i switch {
                    0 => new InteractiveMapPointInfo(name, "Avenida Rudge", true, true, false, false, false),
                    1 => new InteractiveMapPointInfo(name, "Theatro Municipal", true, true, true, true, true),
                    2 => new InteractiveMapPointInfo(name, "Parque do Ibirapuera", true, true, true, false, false),
                    3 => new InteractiveMapPointInfo(name, "Jardim Luzitania", true, true, true, false, true),
                    4 => new InteractiveMapPointInfo(name, "Iguatemi", true, true, true, true, false),
                    5 => new InteractiveMapPointInfo(name, "Rua Lisboa", true, true, true, false, false),
                    6 => new InteractiveMapPointInfo(name, "Cemitério da Consolação", true, true, true, true, true),
                    7 => new InteractiveMapPointInfo(name, "Cemitério da Consolação", true, true, true, true, true)
                };
            });
        }
    }
}