﻿using System;
using PropertyChanged;
using SP.Model;

namespace SP.ViewModel.AdditionalInfo {
    public class BMRCalcVM : Navigable {
        [AlsoNotifyFor(nameof(Index))] public bool IsMan { get; set; } = true;

        [AlsoNotifyFor(nameof(Index))] public int Height { get; set; } = 170;

        [AlsoNotifyFor(nameof(Index))] public int Weight { get; set; } = 70;

        [AlsoNotifyFor(nameof(Index))] public int Age { get; set; } = 30;

        public double Index => IsMan ? 66 + 13.7 * Weight + 5 * Height - 6.8 * Age : 655 + 9.6 * Weight + 1.8 * Height - 4.7 * Age;
    }
}