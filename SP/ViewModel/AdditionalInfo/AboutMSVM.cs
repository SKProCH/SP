﻿using System.Collections.Generic;
using PropertyChanged;
using SP.Model;

namespace SP.ViewModel.AdditionalInfo {
    public class AboutMSVM : Navigable {
        public AboutMSVM() {
            Images = new List<string>() {
                "/Resources/banco-banespa.jpg",
                "/Resources/ibirapuera-park-lake.jpg",
                "/Resources/teatro-municipal.jpg",
                "/Resources/marathon-image.jpg"
            };
        }
        public List<string> Images { get; set; }

        public string CurrentImage => Images[ImageIndex - 1];

        [AlsoNotifyFor(nameof(CurrentImage))]
        public int ImageIndex { get; set; } = 1;
    }
}