﻿using System.Windows;
using System.Windows.Controls;
using SP.Model;
using SP.View.AdditionalInfo;

namespace SP.ViewModel.AdditionalInfo {
    public class AdditionalInfoMainVM : PropertyChangedBase {
        private Window _currentWindow;
        private ContentControl _currentContent;

        public AdditionalInfoMainVM(Window currentWindow) {
            _currentWindow = currentWindow;
        }

        public TimeProvider TimeProvider { get; set; } = TimeProvider.Instance;
        public bool IsMenuVisible { get; set; } = true;

        public ContentControl CurrentContent {
            get => _currentContent;
            set {
                var newDataContext = value?.DataContext;

                if (newDataContext is INavigable newNavigable) {
                    newNavigable.Navigate += NavigableOnNavigate;
                }

                var oldDataContext = _currentContent?.DataContext;

                if (oldDataContext is INavigable oldNavigable) {
                    oldNavigable.Navigate -= NavigableOnNavigate;
                    oldNavigable.OnClosing();
                }

                _currentContent = value;
                IsMenuVisible = false;
            }
        }

        public AnotherCommandImplementation Back =>
            new AnotherCommandImplementation(o => NavigableOnNavigate(CurrentContent?.DataContext ?? this, null));

        public AnotherCommandImplementation OpenInfoAbout => new AnotherCommandImplementation(o => {
            CurrentContent = new InfoAbout {DataContext = new InfoAboutVM()};
        });

        public AnotherCommandImplementation OpenCharitiesList => new AnotherCommandImplementation(o => {
            CurrentContent = new CharityOrganisations {DataContext = new CharityOrganisationsVM()};
        });

        public AnotherCommandImplementation HowLongMarathon => new AnotherCommandImplementation(o => {
            CurrentContent = new HowLongMarathon {DataContext = new HowLongMarathonVM()};
        });

        public AnotherCommandImplementation AboutMS => new AnotherCommandImplementation(o => { CurrentContent = new AboutMS {DataContext = new AboutMSVM()}; });

        public AnotherCommandImplementation OpenBMI => new AnotherCommandImplementation(o => { CurrentContent = new BMICalc {DataContext = new BMICalcVM()}; });

        public AnotherCommandImplementation OpenBMR => new AnotherCommandImplementation(o => { CurrentContent = new BMRCalc {DataContext = new BMRCalcVM()}; });

        public AnotherCommandImplementation PulseCalc => new AnotherCommandImplementation(o => {
            CurrentContent = new PulseCalc {DataContext = new PulseCalcVM()};
        });


        private void NavigableOnNavigate(object sender, object newContent) {
            var navigateTo = newContent ?? (sender is INavigable navigable ? navigable.PreviousContent : null);
            if (navigateTo is ContentControl control) {
                if (control.DataContext is INavigable data) {
                    data.PreviousContent = newContent != null
                        ? CurrentContent
                        : ((data.PreviousContent as ContentControl)?.DataContext as INavigable)?.PreviousContent;
                }

                CurrentContent = control;
            }
            else if (IsMenuVisible) {
                _currentWindow.Close();
            }
            else {
                IsMenuVisible = true;
            }
        }
    }
}