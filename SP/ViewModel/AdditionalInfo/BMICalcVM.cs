﻿using System;
using PropertyChanged;
using SP.Model;

namespace SP.ViewModel.AdditionalInfo {
    public class BMICalcVM : Navigable {
        public bool IsMan { get; set; } = true;

        [AlsoNotifyFor(nameof(Status))] public int Height { get; set; } = 170;

        [AlsoNotifyFor(nameof(Status))] public int Weight { get; set; } = 70;

        public string StatusImage => Status switch {
            BmiStatus.Недостаточный => "/Resources/bmi-underweight-icon.png",
            BmiStatus.Здоровый      => "/Resources/bmi-healthy-icon.png",
            BmiStatus.Избыточный    => "/Resources/bmi-overweight-icon.png",
            BmiStatus.Ожирение      => "/Resources/bmi-obese-icon.png",
            _                       => ""
        };

        [AlsoNotifyFor(nameof(StatusImage))]
        public BmiStatus Status => Index switch {
            var x when x > 30   => BmiStatus.Ожирение,
            var x when x > 25   => BmiStatus.Избыточный,
            var x when x > 18.5 => BmiStatus.Здоровый,
            _                   => BmiStatus.Недостаточный
        };

        [AlsoNotifyFor(nameof(Status))] public double Index => Weight / Math.Pow((double) Height / 100, 2);
    }

    // Да, enum'ы, да и вообще что угодно не на английском - плохой тон
    // Но мне лень писать конвертер
    public enum BmiStatus {
        Недостаточный,
        Здоровый,
        Избыточный,
        Ожирение
    }
}