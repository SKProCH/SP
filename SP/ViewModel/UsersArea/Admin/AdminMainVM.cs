﻿using System;
using SP.Model;
using SP.View.UsersArea.Admin;

namespace SP.ViewModel.UsersArea.Admin {
    public class AdminMainVM : Navigable, IAlertable {
        public UserCredentials UserCredentials { get; set; }

        public AdminMainVM(UserCredentials userCredentials) {
            UserCredentials = userCredentials;
        }

        public AnotherCommandImplementation Users => new AnotherCommandImplementation(o => {
            ExecuteNavigate(new AdminUserManagement {DataContext = new AdminUserManagementVM()});
        });

        public AnotherCommandImplementation Volunteers => new AnotherCommandImplementation(o => { Alert?.Invoke(null, "Функционал еще не реализован"); });

        public AnotherCommandImplementation Inventory => new AnotherCommandImplementation(o => { Alert?.Invoke(null, "Функционал еще не реализован"); });

        public AnotherCommandImplementation CharityOrganisations => new AnotherCommandImplementation(o => {
            ExecuteNavigate(new AdminCharityControl {DataContext = new AdminCharityControlVM()});
        });

        public event EventHandler<string> Alert;
    }
}