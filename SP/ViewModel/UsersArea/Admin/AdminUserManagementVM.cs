﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Documents;
using SP.Model;

namespace SP.ViewModel.UsersArea.Admin {
    public class AdminUserManagementVM : Navigable {
        private int _roleFilterIndex;
        private string _searchText;

        public AdminUserManagementVM() {
            LoadList();
            UpdateFilters();
        }

        public ObservableCollection<string> RoleFilterList { get; set; } = new ObservableCollection<string>();

        public int RoleFilterIndex {
            get => _roleFilterIndex;
            set {
                _roleFilterIndex = value;
                UpdateFilters();
            }
        }

        public string SearchText {
            get => _searchText;
            set {
                _searchText = value;
                UpdateFilters();
            }
        }

        public AnotherCommandImplementation Update => new AnotherCommandImplementation(async o => {
            LoadList();
            await UpdateFilters();
        });

        public ObservableCollection<User> UsersSource { get; set; } = new ObservableCollection<User>();

        private List<User> Users { get; set; } = new List<User>();

        public void LoadList() {
            Users = File.ReadAllText("Users.csv").Split('\n').Select(s => new User(s.Trim())).ToList();
            var groups = Users.GroupBy(user => user.Role).Select(users => users.First()).Select(user => user.Role).ToList();
            groups.Insert(0, "Все пользователи");
            RoleFilterList = new ObservableCollection<string>(groups);
            RoleFilterIndex = 0;
        }

        public async Task UpdateFilters() {
            var temp = Users.ToList();
            await Task.Run(() => {
                if (RoleFilterIndex > 0) {
                    var roleName = RoleFilterList[RoleFilterIndex];
                    temp = temp.Where(s => s.Role == roleName).ToList();
                }

                if (!string.IsNullOrWhiteSpace(SearchText)) {
                    temp = temp.Where(user => user.FirstName.ToLower().Contains(SearchText) ||
                                              user.LastName.ToLower().Contains(SearchText) ||
                                              user.Email.Contains(SearchText))
                               .ToList();
                }
            });

            UsersSource = new ObservableCollection<User>(temp);
        }
    }
}