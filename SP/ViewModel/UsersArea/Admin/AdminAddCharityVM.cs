﻿using System;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using SP.Model;

namespace SP.ViewModel.UsersArea.Admin {
    public class AdminAddCharityVM : PropertyChangedBase {
        private string _logoPath;
        public CharityOrganisation CharityOrganisation { get; set; }

        public AdminAddCharityVM(CharityOrganisation organisation = null) {
            CharityOrganisation = organisation ?? CharityOrganisation.Create("", "", "");
        }

        public AnotherCommandImplementation Save =>
            new AnotherCommandImplementation(o => {
                if (LogoChoosen) {
                    var destFileName = Path.GetFullPath( $"Resources/Charities/{Guid.NewGuid().ToString()}.img");
                    Directory.CreateDirectory(Path.GetDirectoryName(destFileName));
                    File.Copy(LogoPath,  destFileName);
                    Logo = new BitmapImage(new Uri(destFileName));
                }
                CharityOrganisation.Logo = Logo;
                DialogHost.CloseDialogCommand.Execute(CharityOrganisation, null);
            });

        public AnotherCommandImplementation Cancel => new AnotherCommandImplementation(o => { DialogHost.CloseDialogCommand.Execute(null, null); });

        public AnotherCommandImplementation Browse => new AnotherCommandImplementation(o => {
            var openFileDialog = new OpenFileDialog {Filter = BitmapImageCheck.GetFilters()};
            if (openFileDialog.ShowDialog() == true) {
                LogoPath = openFileDialog.FileName;
            }
        });

        public string LogoPath {
            get => _logoPath;
            set {
                _logoPath = value;
                try {
                    Logo = new BitmapImage(new Uri(value));
                    LogoChoosen = true;
                }
                catch (Exception) {
                    Logo = new BitmapImage();
                    LogoChoosen = false;
                }
            }
        }
        
        public bool LogoChoosen { get; set; }

        public BitmapImage Logo { get; set; }
    }
}