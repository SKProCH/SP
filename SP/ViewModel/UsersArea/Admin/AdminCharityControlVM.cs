﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using MaterialDesignThemes.Wpf;
using SP.Model;
using SP.View.UsersArea.Admin;

namespace SP.ViewModel.UsersArea.Admin {
    public class AdminCharityControlVM : Navigable {
        public AdminCharityControlVM() {
            Charities = new ObservableCollection<CharityOrganisation>(File.ReadAllText("Resources/Charity.csv")
                                                                          .Split(new[] {"\n"}, StringSplitOptions.RemoveEmptyEntries)
                                                                          .SafeSelect(CharityOrganisation.ParseCharityCsvLine));
        }

        public AnotherCommandImplementation Add => new AnotherCommandImplementation(async o => {
            var result = await DialogHost.Show(new AdminAddCharity {DataContext = new AdminAddCharityVM()});
            if (result != null) {
                Charities.Add((CharityOrganisation) result);
                SaveCharities();
            }
        });

        public ObservableCollection<CharityOrganisation> Charities { get; set; }

        private void SaveCharities() {
            // Давайте будем откровенны
            // Никому ID не нужно
            // Формат csv - говно
            // И та информация, которая дается в файле вместе с лекцией в формате csv, представлена ни разу не в cvs
            File.WriteAllText("Resources/Charity.csv",
                string.Join("\n", Charities.Select((organisation, i) => $"{i}|{organisation.Name}|{organisation.Description}|{organisation.Logo.UriSource}")));
        }
    }
}