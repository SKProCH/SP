﻿using System;
using SP.Model;
using SP.View.UsersArea.Coordinators;

namespace SP.ViewModel.UsersArea.Coordinators {
    
    public class CoordinatorsMainVM : Navigable, IAlertable {
        public UserCredentials UserCredentials { get; set; }
        public CoordinatorsMainVM(UserCredentials userCredentials) {
            UserCredentials = userCredentials;
        }
        
        public event EventHandler<string> Alert;

        public AnotherCommandImplementation Runners => new AnotherCommandImplementation(o => {
            Alert?.Invoke(null, "Функционал еще не реализован");
        });

        public AnotherCommandImplementation Sponsors => new AnotherCommandImplementation(o => {
            ExecuteNavigate(new CoordinatorSponsorship {DataContext = new CoordinatorSponsorshipVM()});
        });
    }
}