﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using SP.Model;

namespace SP.ViewModel.UsersArea.Coordinators {
    public class CoordinatorSponsorshipVM : Navigable {
        public CoordinatorSponsorshipVM() {
            YearsList = File.ReadAllText("Resources/Sponsorship.csv").Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries).Skip(1)
                            .Select(s => new Sponsorship(s)).ToList();
            foreach (var sponsorship in YearsList) {
                sponsorship.EnabledChanged += (sender, args) => { ApplyFilters(); };
            }

            
            ApplyFilters();
        }

        public List<Sponsorship> YearsList { get; set; }

        public void ApplyFilters() {
            var chartModel = new PlotModel();
            chartModel.Series.Add(new BarSeries {
                RenderInLegend = true,
                ItemsSource = YearsList.Where(sponsorship => sponsorship.IsEnabled).Select(sponsorship => new BarItem(sponsorship.Amount)),
                LabelPlacement = LabelPlacement.Inside,
                LabelFormatString = "{0:.00}￥"
            });

            chartModel.Axes.Add(new CategoryAxis {
                Position = AxisPosition.Left,
                Key = "Axis",
                ItemsSource = YearsList.Where(sponsorship => sponsorship.IsEnabled).Select(sponsorship => sponsorship.Name)
            });

            ChartModel = chartModel;
        }

        public PlotModel ChartModel { get; set; }
    }
}