﻿using System;
using SP.Model;
using SP.View.UsersArea.Runners;

namespace SP.ViewModel.UsersArea.Runners {
    public class RunnersMainVM : Navigable, IAlertable {
        public UserCredentials UserCredentials { get; set; }
        public RunnersMainVM(UserCredentials userCredentials) {
            UserCredentials = userCredentials;
        }
        
        public AnotherCommandImplementation RegisterToMarathon => new AnotherCommandImplementation(o => {
            var registerToMarathon = new RegisterToMarathon {DataContext = new RegisterToMarathonVM(UserCredentials)};
            ExecuteNavigate(registerToMarathon);
        });

        public AnotherCommandImplementation EditProfile => new AnotherCommandImplementation(o => {
            Alert?.Invoke(null, "Функционал еще не реализован");
        });

        public AnotherCommandImplementation Contacts => new AnotherCommandImplementation(o => {
            Alert?.Invoke(null, "Функционал еще не реализован");
        });

        public AnotherCommandImplementation Results => new AnotherCommandImplementation(o => {
            Alert?.Invoke(null, "Функционал еще не реализован");
        });

        public AnotherCommandImplementation ShowSponsor => new AnotherCommandImplementation(o => {
            Alert?.Invoke(null, "Функционал еще не реализован");
        });

        public event EventHandler<string> Alert;
    }
}