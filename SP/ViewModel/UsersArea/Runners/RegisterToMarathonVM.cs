﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Documents;
using PropertyChanged;
using SP.Model;

namespace SP.ViewModel.UsersArea.Runners {
    // ReSharper disable once InconsistentNaming
    public class RegisterToMarathonVM : Navigable {
        private UserCredentials UserCredentials;
        public RegisterToMarathonVM(UserCredentials userCredentials) {
            UserCredentials = userCredentials;
            FondsList.Add("Фонд 1");
            FondsList.Add("Фонд 2");
            FondsList.Add("Фонд 3");
            FondsList.Add("Фонд кошек");
        }
        
        [AlsoNotifyFor(nameof(TotalPrice), nameof(CanRegister))]
        public bool IsFullMarathon { get; set; } = true;

        [AlsoNotifyFor(nameof(TotalPrice), nameof(CanRegister))]
        public bool IsSemiMarathon { get;set;}

        [AlsoNotifyFor(nameof(TotalPrice), nameof(CanRegister))]
        public bool IsSmallMarathon { get;set; }

        
        [AlsoNotifyFor(nameof(TotalPrice))]
        public bool PackA { get; set; } = true;

        [AlsoNotifyFor(nameof(TotalPrice))]
        public bool PackB { get;set; }

        [AlsoNotifyFor(nameof(TotalPrice))]
        public bool PackC { get;set; }

        public ObservableCollection<string> FondsList { get; set; } = new ObservableCollection<string>();

        [AlsoNotifyFor(nameof(TotalPrice))]
        public int PaidAmount { get; set; }

        public int SelectedFondIndex { get; set; } = 0;

        [AlsoNotifyFor(nameof(CanRegister))]
        public bool PaidAmountError { get; set; } = false;

        public bool CanRegister => !PaidAmountError && (IsFullMarathon || IsSemiMarathon || IsSmallMarathon);

        public AnotherCommandImplementation Register => new AnotherCommandImplementation(o => {
            ShowRegisterFinal = true;
        });

        public AnotherCommandImplementation Cancel => new AnotherCommandImplementation(o => {
            ExecuteReturn();
        });

        public string TotalPrice {
            get {
                var price = 0;
                if (PackA) price = 0;
                if (PackB) price = 20;
                if (PackC) price = 40;

                if (IsFullMarathon) price += 145;
                if (IsSemiMarathon) price += 75;
                if (IsSmallMarathon) price += 20;

                price += PaidAmount;
                return price + "$";
            }
        }

        public bool ShowRegisterFinal { get; set; } = false;

        [DependsOn(nameof(SelectedFondIndex))]
        public string SelectedFond => FondsList[SelectedFondIndex];
    }
}