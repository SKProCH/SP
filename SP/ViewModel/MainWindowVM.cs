﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Documents;
using SP.Model;
using SP.View;
using SP.View.AdditionalInfo;
using SP.ViewModel.AdditionalInfo;

namespace SP.ViewModel {
    public class MainWindowVM : PropertyChangedBase {
        public TimeProvider TimeProvider { get; set; } = TimeProvider.Instance;

        public AnotherCommandImplementation OpenRunners => new AnotherCommandImplementation(o => {
            if (!(o is Window window)) return;
            var runners = new Runners {DataContext = new RunnersVM()};
            window.Hide();
            runners.ShowDialog();
            window.Show();
        });

        public AnotherCommandImplementation StartAuth => new AnotherCommandImplementation(o => {
            if (!(o is Window window)) return;
            var userArea = new UserArea();
            userArea.DataContext = new UserAreaVM(userArea);
            window.Hide();
            userArea.ShowDialog();
            window.Show();
        });

        public AnotherCommandImplementation OpenAdditionalInfo => new AnotherCommandImplementation(o => {
            if (!(o is Window window)) return;
            var userArea = new AdditionalInfoMain();
            userArea.DataContext = new AdditionalInfoMainVM(userArea);
            window.Hide();
            userArea.ShowDialog();
            window.Show();
        });

        public AnotherCommandImplementation OpenAboutWSChina => new AnotherCommandImplementation(o => {
            if (!(o is Window window)) return;
            var newWindow = new AboutWSChina {DataContext = new AboutWSChinaVM()};
            window.Hide();
            newWindow.ShowDialog();
            window.Show();
        });
    }
}