﻿using System;
using System.Windows;
using System.Windows.Controls;
using MaterialDesignThemes.Wpf;
using SP.Model;
using SP.View.UsersArea.Admin;
using SP.View.UsersArea.Coordinators;
using SP.View.UsersArea.Runners;
using SP.ViewModel.UsersArea.Admin;
using SP.ViewModel.UsersArea.Coordinators;
using SP.ViewModel.UsersArea.Runners;

namespace SP.ViewModel {
    public class UserAreaVM : PropertyChangedBase {
        public UserAreaVM(Window currentWindow) {
            _currentWindow = currentWindow;
        }

        private ContentControl _currentContent;
        private Window _currentWindow;

        public AnotherCommandImplementation Close => new AnotherCommandImplementation(o => {
            if (o is Window window) {
                window.Close();
            }
        });

        public TimeProvider TimeProvider { get; set; } = TimeProvider.Instance;

        public bool IsAuthVisible { get; set; } = true;

        public string Login { get; set; }

        public SnackbarMessageQueue MyMessageQueue { get; set; } = new SnackbarMessageQueue(TimeSpan.FromSeconds(2));

        public AnotherCommandImplementation StartLogin => new AnotherCommandImplementation(o => {
            // Нарушение MVVM, но мне лень мутить интерфейсы или бехейверы
            if (!(o is PasswordBox passwordBox)) return;
            var passwordErrors = UserProvider.ValidatePassword(passwordBox.Password);
            if (passwordErrors != null) {
                MyMessageQueue.Enqueue(passwordErrors);
                return;
            }

            var user = UserProvider.Login(Login, passwordBox.Password);
            if (user == null) {
                MyMessageQueue.Enqueue("Неправильные данные для входа");
                return;
            }

            ProceedAuth(user);
        });

        private void ProceedAuth(UserCredentials user) {
            IsAuthVisible = false;
            #pragma warning disable 8509
            CurrentContent = user.Type switch {
                #pragma warning restore 8509
                UserType.Runner      => new RunnersMain {DataContext = new RunnersMainVM(user)},
                UserType.Coordinator => new CoordinatorsMain {DataContext = new CoordinatorsMainVM(user)},
                UserType.Admin       => new AdminMain {DataContext = new AdminMainVM(user)}
            };
        }


        public ContentControl CurrentContent {
            get => _currentContent;
            set {
                var newDataContext = (value as ContentControl)?.DataContext;
                if (newDataContext is IAlertable newAlertable) {
                    newAlertable.Alert += OnAlert;
                }

                if (newDataContext is INavigable newNavigable) {
                    newNavigable.Navigate += NavigableOnNavigate;
                }

                var oldDataContext = (_currentContent as ContentControl)?.DataContext;
                if (oldDataContext is IAlertable oldAlertable) {
                    oldAlertable.Alert -= OnAlert;
                }

                if (oldDataContext is INavigable oldNavigable) {
                    oldNavigable.Navigate -= NavigableOnNavigate;
                    oldNavigable.OnClosing();
                }

                _currentContent = value;
            }
        }

        private void NavigableOnNavigate(object sender, object newContent) {
            var navigateTo = newContent ?? (sender is INavigable navigable ? navigable.PreviousContent : null);
            if (navigateTo is ContentControl control) {
                if (control.DataContext is INavigable data) {
                    data.PreviousContent = newContent != null
                        ? CurrentContent
                        : ((data.PreviousContent as ContentControl)?.DataContext as INavigable)?.PreviousContent;
                }

                CurrentContent = control;
            }
            else {
                _currentWindow.Close();
            }
        }

        private void OnAlert(object sender, string e) {
            MyMessageQueue.Enqueue(e);
        }

        public AnotherCommandImplementation Back => new AnotherCommandImplementation(o => { NavigableOnNavigate(CurrentContent?.DataContext ?? this, null); });
    }
}