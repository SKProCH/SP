﻿using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Windows;
using SP.ViewModel;

namespace SP {
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App {
        protected override void OnStartup(StartupEventArgs e) {
            FrameworkElement.StyleProperty.OverrideMetadata(typeof(Window), new FrameworkPropertyMetadata {
                DefaultValue = FindResource(typeof(Window))
            });

            var mainWindow = new MainWindow() {DataContext = new MainWindowVM()};
            mainWindow.Show();
        }
    }
}